@extends('layouts.admin')
@section('content')
	<h1>Posts</h1>
	<table class="table">
	  <tr>
	    <th>id</th>
	  	<th>photo_id</th>
	    <th>owner</th>
	    <th>category</th>
	    <th>title</th>
	    <th>body</th>
	    <th>created</th>
	    <th>updated</th>
	  </tr>
	  @if($posts)
	  	@foreach($posts as $post)
	  <tr>
	    <td>{{$post->id}}</td>
	  	<td><img width="50" src="{{URL::to('/')}}/images/{{$post->photo ? $post->photo->file : 'nophoto.png'}}" alt=""></td>
	    <td>{{$post->user->name}}</td>
	    <td>{{$post->category ? $post->category->name : 'Uncategorized'}}</td>
	    <td><a href="{{route('admin.posts.edit', $post->id)}}">{{$post->title}}</a></td>
	    <td>{{str_limit($post->body, 30)}}</td>
	    <td>{{$post->created_at->diffForhumans()}}</td>
	    <td>{{$post->updated_at->diffForhumans()}}</td>
	  </tr>
	  	@endforeach
	  @endif
	</table>
@endsection
