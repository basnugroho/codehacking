@extends('layouts.admin')
@section('content')
	<h1>Edit Posts</h1>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-sm-9">
            <img class="text-center" width="400" src="{{URL::to('/')}}/images/{{$posts->photo ? $posts->photo->file : 'nophoto.png'}}" alt="">
        </div>

        <div class="col-sm-12">
            {!! Form::model($posts, ['method'=>'PATCH', 'action'=>['AdminPostsController@update', $posts->id], 'files'=>true]) !!}
            <div class="form-group">
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('category_id', 'Category:') !!}
                {!! Form::select('category_id', $categories, null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('photo_id', 'Photo:') !!}
                {!! Form::file('photo_id', null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('body', 'Description:') !!}
                {!! Form::textarea('body', null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Save', ['class'=>'btn btn-primary col-sm-6']) !!}
            </div>
            {!! Form::close() !!}
            {!! Form::open(['method'=>'DELETE', 'action'=>['AdminPostsController@destroy', $posts->id]]) !!}
                <div class="form-group">
                    {!! Form::submit('Delete Post', ['class'=>'btn btn-danger col-sm-6']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
	<div class="row">
		@include('includes.form_error')
	</div>
@endsection
